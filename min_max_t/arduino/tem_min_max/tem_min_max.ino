

#include <DHT.h>    // importa la Librerias DHT
#include <DHT_U.h>
#include <EEPROM.h>       // libreria EEPROM
#include <Wire.h>    // incluye libreria para interfaz I2C
#include <RTClib.h>   // incluye libreria para el manejo del modulo RTC

RTC_DS3231 rtc;     // crea objeto del tipo RTC_DS3231

int SENSOR = 2;     // pin DATA de DHT22 a pin digital 2
int TEMPERATURA = 28;
int HUMEDAD;
int SUP = 40;
int INF = -1;
int LED = 8;
int MAXT = EEPROM.read(5) ;
int MINT = EEPROM.read(6) ;

DHT dht(SENSOR, DHT22);   // creacion del objeto, cambiar segundo parametro
        // por DHT11 si se utiliza en lugar del DHT22
void setup(){
  Serial.begin(9600);   // inicializacion de monitor serial
  dht.begin();      // inicializacion de sensor
  pinMode(LED, OUTPUT);
  if (! rtc.begin()) {       // si falla la inicializacion del modulo
 Serial.println("Modulo RTC no encontrado !");  // muestra mensaje de error
 while (1);         // bucle infinito que detiene ejecucion del programa
 }
}

void loop(){
  DateTime fecha = rtc.now();
  TEMPERATURA = dht.readTemperature();  // obtencion de valor de temperatura
  HUMEDAD = dht.readHumidity();   // obtencion de valor de humedad

  //delay(1000);
   
    
  //Serial.print("Temperatura: ");  // escritura en monitor serial de los valores
  Serial.print(TEMPERATURA);
  if (TEMPERATURA >= 30) {
    digitalWrite(LED, HIGH);
    }
    else
    {
      digitalWrite(LED, LOW);
      }
  if (MAXT >= TEMPERATURA){    
    }
    else {
      MAXT = TEMPERATURA;
      EEPROM.write (5, MAXT);
      }
           
  if (MINT >= TEMPERATURA){
    MINT = TEMPERATURA ;
    EEPROM.write (6, MINT);
    }
    else {
    }

  if (fecha.minute() == 23){
    digitalWrite(LED, HIGH);
   }
    else {
      digitalWrite(LED, LOW);
      }
    
  Serial.print(",");
  Serial.print(EEPROM.read(5));
  Serial.print(",");
  Serial.print(EEPROM.read(6));
  Serial.print(",");
  Serial.print(HUMEDAD);
  Serial.print(",");
  Serial.print(fecha.hour());      // funcion que obtiene la hora de la fecha completa
 Serial.print(",");       // caracter dos puntos como separador
 Serial.print(fecha.minute());      // funcion que obtiene los minutos de la fecha completa
 Serial.print(",");       // caracter dos puntos como separador
 Serial.print(fecha.second());
 Serial.println(",");

  delay(500);
}
