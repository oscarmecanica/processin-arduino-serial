

#include <EEPROM.h>       // libreria EEPROM
int LED = 8;

void setup() {
  Serial.begin(9600);       // inicializa monitor serie a 9600 bps
  
  Serial.print("Capacidad de memoria: "); // imprime texto 
  Serial.println( EEPROM.length() );    // obtiene capacidad de memoria EEPROM y muestra
  Serial.println(" ");        // linea en blanco

  Serial.print("Valor almacenado en direccion 0: ");  // imprime texto
  Serial.println( EEPROM.read(0) );         // lee direccion cero y muestra
  pinMode(LED, OUTPUT);
  if (EEPROM.read(0) == 39){
    digitalWrite(LED, HIGH);
    Serial.println("39");
    }

  Serial.print("Almacenando numero 39 en direccion 0"); // imprime texto
  EEPROM.write(0, 38);                      // escribe en direccion cero el numero 39
}

void loop() {         // funcion loop() declarada pero sin contenido
  // nada por aqui
}
